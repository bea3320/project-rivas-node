const express = require("express");
const playersController = require("../controllers/players.controller");
const Player = require("../models/Player");
const authenticate = require("../middlewares/auth.middleware");
const router = express.Router();
const {
  upload,
  uploadToCloudinary,
} = require("../middlewares/files.middleware");

router.get("/", playersController.playersGet);

router.get(
  "/create-player",
  [authenticate.isAuthenticated],
  playersController.playersCreateGet
);

router.post(
  "/create",
  [upload.single("avatar"), uploadToCloudinary],
  playersController.playersCreatePost
);

router.get("/name/:name", playersController.playerByNameGet);

router.get("/dorsal/:dorsal", playersController.playerByDorsalGet);

router.get("/position/:position", playersController.playerByPositionGet);

router.get("/category/:category", playersController.playerByCategoryGet);

router.get("/year/:year", playersController.playerByYearGet);

router.get(
  "/modify/:id",
  [authenticate.isAuthenticated],
  playersController.playerModifyGet
);

router.post(
  "/modify/:id",
  [upload.single("avatar"), uploadToCloudinary, authenticate.isAuthenticated],
  playersController.playerModifyPost
);

router.put("/modify/", playersController.playerModifyPut);

router.post("/:id", [authenticate.isAuthenticated], playersController.playerDeletedPost);

router.delete("/:id", playersController.playerDeletedDelete);

router.get("/:id", playersController.playerByIdGet);

module.exports = router;
