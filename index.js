require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const passport = require('passport');
const session = require('express-session');
const MongoStore = require('connect-mongo');
const path = require('path');
const db = require('./db.js');
const indexRoutes = require('./routes/index.routes');
const playersRoutes = require('./routes/players.routes');
const garmentsRoutes = require('./routes/garments.routes');
const authRoutes = require('./routes/auth.routes');
const cartRoutes = require('./routes/cart.routes');
const {isAuthenticated } = require('./middlewares/auth.middleware');

db.connect();
require('./db');
const PORT = process.env.PORT || 3000;

const app = express();
const router = express.Router();

require('./passport/passport');

app.use(session({
    secret:process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: false,
    cookie: {
        maxAge: 48 * 60 * 60 *100
    },
    store: MongoStore.create({ mongoUrl: db.DB_URL }),
}));

app.use(passport.initialize());
app.use(passport.session());

app.use(express.static(path.join(__dirname, 'public')));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use((req, res, next) => {
    console.log('REQ.ORIGINAL URL EN INDEX.JS', req.originalUrl);
    next();
});

app.use('/', router);
app.use('/', indexRoutes);
app.use('/players',/* [isAuthenticated], */ playersRoutes);
app.use('/garments', garmentsRoutes);
app.use('/auth', authRoutes);
app.use('/cart', cartRoutes);
app.get('/favicon.ico', (req, res) => res.status(204));

app.use('*', (req, res, next) => {
    const error = new Error('Route not found');
    error.status=404;
    next(error);
});

/* app.use('/', router); */

app.use('*', (req, res, next) => {
    const error = new Error('Route not found');
    error.status = 404;
    next(error);
});

app.use((error, req, res, next) => {
    console.log(error);
    return res.status(error.status || 500).render('error', {error: error.message || 'Unexpected error'});
});

app.listen(PORT, () => {
    console.log(`Server running in http://localhost:${PORT}`)
});

