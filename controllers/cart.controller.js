const Cart = require('../models/Cart');


const cartGet = async ( req, res, next )=> {
    return res.render("cart");
}

module.exports = {
    cartGet
}