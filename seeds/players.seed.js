const mongoose = require("mongoose");
require('dotenv'), config();
const db = require("../db");
const Player = require("../models/Player");

const players = [
  {
    name: "Mario",
    dorsal: 24,
    position: "Delantero",
    category: "Benjamín",
    year: 2011,
    image: "https://www.rivasfutbolclub.com/images/ba/_JRR4858.jpg",
  },

  {
    name: "Asier",
    dorsal: 1,
    position: "Portero",
    category: "Benjamín",
    year: 2011,
    image: "https://www.rivasfutbolclub.com/images/plantillas/aficionado/JOTA.jpg",
  },
  {
    name: "Lucas",
    dorsal: 3,
    position: "Defensa",
    category: "Benjamín",
    year: 2011,
    image: "https://www.rivasfutbolclub.com/images/ba/_JRR4898.jpg",
  },
];

mongoose
  .connect(db.DB_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(async () => {
    //Buscar los jugadores existentesen nuestra base de datos
    //Si tenemos juagadores, las borramos, Si no tenemos jugadores, continuamos ejecutando.
    const allPlayers = await Player.find();
    console.log(allPlayers);

    if (allPlayers.length) {
      console.log("Deleting all Players...");
      await Player.collection.drop();
    }
  })
  .catch((error) => {
    //Gestionaremos nuestro error
    console.log("Error deleting data: ", error);
  })
  .then(async () => {
    //Añadimos a nuestra base de datos los jnugadores del seed.
    await Player.insertMany(players);
    /* console.log("Successfully added players to DB..."); */
  })
  .catch((error) => {
    //Gestionaremos nuestro error.
    console.log("Error creating data: ", error);
  })
  //Cerramos la conexion con base de datos.
  .finally(() => mongoose.disconnect());
