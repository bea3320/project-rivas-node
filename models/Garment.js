const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const garmentSchema = new Schema({
    clothing:{type: String, required: true},
    description: {type: String},
    price: {type: Number, required: true},
    img: {type: String}
}, {
    timestamps: true,
});

const Garment = mongoose.model('Garment', garmentSchema);

module.exports = Garment;