const mongoose = require("mongoose");
const db = require("../db");
const Garment = require("../models/Garment");

const garments = [
  {
    clothing: "Camiseta de juego",
    price: 20.00,
    description: "Camiseta ligera y transpirable",
    img: "https://www.rivasfutbolclub.com/images/virtuemart/product/l1.png",
  },
  {
    clothing: "Pantalón de juego",
    price: 8.00,
    description: "Pantalón transpirable y ligero",
    img: "https://www.rivasfutbolclub.com/images/virtuemart/product/9.png",
  },
  {
    clothing: "Medias",
    price: 20.00,
    description: "Medias 100% algodón",
    img: "https://www.rivasfutbolclub.com/images/virtuemart/product/11.png",
  },
  {
    clothing: "Chubasquero",
    price: 17.00,
    description: "100% Impermeable",
    img: "https://www.rivasfutbolclub.com/images/virtuemart/product/resized/10_0x90.png",
  },
  {
    clothing: "Chándal",
    price: 45.00,
    description: "100% Algodón",
    img: "https://www.rivasfutbolclub.com/images/virtuemart/product/resized/7_0x90.png",
  },
  {
    clothing: "Mochila",
    price: 22.00,
    description: "Mochila de gran capacidad",
    img: "https://www.rivasfutbolclub.com/images/virtuemart/product/6.png",
  },
];

mongoose
  .connect(db.DB_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(async () => {
    const allGarments = await Garment.find();
    console.log(allGarments);

    if (allGarments.length) {
      console.log("Deleting all Garments...");
      await Garment.collection.drop();
    }
  })
  .catch((error) => {
    console.log("Error deleting data: ", error);
  })
  .then(async () => {
    await Garment.insertMany(garments);
    console.log("Successfully added garments to DB...");
  })
  .catch((error) => {
    console.log("Error creating data: ", error);
  })

  .finally(() => mongoose.disconnect());
