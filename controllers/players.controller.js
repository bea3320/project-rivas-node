const { findById } = require("../models/Player");
const Player = require("../models/Player");

const playersGet = async (req, res, next) => {
  try {
    const players = await Player.find();
    /* console.log(req.user);
      console.log(!!req.user); */
    return res.render("players", { players, isLogged: !!req.user });
  } catch (error) {
    console.log(error);
    next(new Error(error));
  }
};

const playersCreateGet = (req, res, next) => {
  return res.render("create-player");
};

const playersCreatePost = async (req, res, next) => {
  try {
    const {
      name,
      dorsal,
      position,
      category,
      year,
      creatorId = req.user._id,
    } = req.body;

    const image = req.file_url;

    /*  let image;
    if (req.file) {
      image = req.file.filename;
    } */
    const newPlayer = await new Player({
      name,
      dorsal,
      position,
      category,
      year,
      image,
      creatorId,
    });
    const createdPlayer = await newPlayer.save();
    return res.redirect("/players");
  } catch (error) {
    next(new Error(error));
  }
};

const playerByIdGet = async (req, res, next) => {
  try {
    const { id } = req.params;
    const player = await Player.findById(id);
    if (player) {
      return res.render("player", { player });
    }
    return res.status(404).json("No player found for this id");
  } catch (error) {
    next(new Error(error));
  }
};

const playerByNameGet = async (req, res, next) => {
  try {
    const { name } = req.params;
    const playersByName = await Player.find({ name });
    return res.json(playersByName);
  } catch (error) {
    next(new Error(error));
  }
};

const playerByDorsalGet = async (req, res, next) => {
  try {
    const { dorsal } = req.params;
    const playersByDorsal = await Player.find({ dorsal });
    return res.json(playersByDorsal);
  } catch (error) {
    next(new Error(error));
  }
};

const playerByPositionGet = async (req, res, next) => {
  try {
    const { position } = req.params;
    const playersByPosition = await Player.find({ position });
    return res.json(playersByPosition);
  } catch (error) {
    next(new Error(error));
  }
};

const playerByCategoryGet = async (req, res, next) => {
  try {
    const { category } = req.params;
    const playersByCategory = await Player.find({ category });
    return res.json(playersByCategory);
  } catch (error) {
    next(new Error(error));
  }
};

const playerByYearGet = async (req, res, next) => {
  try {
    const { year } = req.params;
    const playersByYear = await Player.find({ year: { $lte: year } });
    return res.json(playersByYear);
  } catch (error) {
    next(new Error(error));
  }
};

const playerModifyGet = async (req, res, next) => {
  try {
    console.log("playerModify");
    const player = await Player.findById(req.params.id);
    console.log(req.params);
    return res.render("modify-player", {
      player,
    });
  } catch (error) {
    next(error);
  }
};

const playerModifyPost = async (req, res, next) => {
  try {
    const { id } = req.params;
    const image = req.file_url;
    const updatePlayer = await Player.findByIdAndUpdate(
      id,
      {
        name: req.body.name,
        dorsal: req.body.dorsal,
        position: req.body.position,
        category: req.body.category,
        year: req.body.year,
        image,
      },
      {
        new: true,
      }
    );
    console.log("UPDATED PLAYER", updatePlayer);
    return res.redirect("/players" /*, {
      updatePlayer,
      isLogged: !!req.user,
    }*/);
  } catch (error) {
    next(error);
  }
};

const playerModifyPut = async (req, res, next) => {
  try {
    const { id } = req.body;
    const updatePlayer = await Player.findByIdAndUpdate(
      id,
      {
        name: req.body.name,
        dorsal: req.body.dorsal,
        position: req.body.position,
        category: req.body.category,
        year: req.body.year,
        image: req.body.image,
      },
      {
        new: true,
      }
    );
    return res.json(updatePlayer);
  } catch (error) {
    next(error);
  }
};

const playerDeletedPost = async (req, res, next) => {
  try {
    const { id } = req.params;
    const deleted = await Player.findByIdAndDelete(id);

    if (deleted) {
      const players = await Player.find();
      return res.render("players", { players, isLogged: !!req.user });
    }
    return res.status(200).json("Player not found");
  } catch (error) {
    next(new Error(error));
  }
};

const playerDeletedDelete = async (req, res, next) => {
  try {
    const { id } = req.params;
    const deleted = await Player.findByIdAndDelete(id);
    if (deleted) {
      return res.status(200).json("Player deleted");
    }
    return res.status(200).json("Player not Found");
  } catch (error) {
    next(error);
  }
};

module.exports = {
  playersGet,
  playersCreateGet,
  playersCreatePost,
  playerByIdGet,
  playerByNameGet,
  playerByDorsalGet,
  playerByPositionGet,
  playerByCategoryGet,
  playerByYearGet,
  playerModifyGet,
  playerModifyPost,
  playerModifyPut,
  playerDeletedPost,
  playerDeletedDelete,
};
