const passport = require("passport");

const registerGet = (req, res, next) => {
  return res.render("register");
};

// /auth/register
const registerPost = (req, res, next) => {
  const { email, password } = req.body;
  console.log("Registrando usuario...", req.body);

  if (!email || !password) {
    const error = new Error("User and password are required");
    return res.render("register", { error: error.message });
  }
  passport.authenticate("registro", (error, user) => {
    if (error) {
      return res.render("register", { error: error.message });
    }
    return res.redirect("/auth/login");
  })(req);
};

const loginGet = (req, res, next) => {
  return res.render("login");
};

// /auth/login
const loginPost = (req, res, next) => {
  const { email, password } = req.body;
  console.log("Logueando usuario...", req.body);

  if (!email || !password) {
    const error = new Error("User and password are required");
    return res.render("login", { error: error.message });
  }

  passport.authenticate("acceso", (error, user) => {
    if (error) {
      return res.render("login", { error: error.message });
    }

    //return res.json(user);

    req.login(user, (error) => {
      if (error) {
        return res.render("login", { error: error.message });
      }
      return res.redirect("/");
    });
  })(req, res, next);
};

const logoutPost = (req, res, next) => {
  console.log("req.user", req.user);

  if (req.user) {
    req.logout();

    req.session.destroy(() => {
      res.clearCookie("connect.sid");
      return res.render("index", { message: "Usuario Deslogueado con éxito" });
    });
  } else {
    return res.redirect("/");
  }
};

module.exports = {
  registerGet,
  registerPost,
  loginGet,
  loginPost,
  logoutPost,
};
