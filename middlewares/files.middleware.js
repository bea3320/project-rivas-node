const multer = require("multer");
const path = require("path");
const fs = require("fs");
const cloudinary = require("cloudinary").v2;
/* console.log('CLOUDINARY', process.env.CLOUDINARY_URL); */

const VALID_TYPE_FILES = ["image/png", "image/jpg", "image/jpeg"];

const storage = multer.diskStorage({
  filename: (req, file, cb) => {
    cb(null, `${Date.now()}-${file.originalname}`);
  },
  destination: (req, file, cb) => {
    cb(null, path.join(__dirname, "../public/uploads"));
  },
});

const fileFilter = (req, file, cb) => {
  if (VALID_TYPE_FILES.includes(file.mimetype)) {
    cb(null, true);
  } else {
    const error = new Error("Invalidad file type!");
    cb(error);
  }
};

const upload = multer({
  storage,
  fileFilter,
});

const uploadToCloudinary = async (req, res, next) => {
  /* console.log(req.file); */
  if (req.file) {
    //subir a cloudinary
    try {
      const filePath = req.file.path;
      const image = await cloudinary.uploader.upload(filePath);
      await fs.unlinkSync(filePath);
      req.file_url =
        image.secure_url ||
        "https://www.rivasfutbolclub.com/images/ba%20-%20lega/_DSC2196.JPG";
      return next();
    } catch (error) {
      return next(error);
    }
  } else {
    //no subir a cloudinary
    return next();
  }
};

module.exports = { upload, uploadToCloudinary };
