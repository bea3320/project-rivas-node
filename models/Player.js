const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const playerSchema = new Schema({
    name:{type: String, required: true},
    dorsal: {type: Number, required: true},
    position: {type: String, required: true},
    category: {type: String, required: true},
    year: {type: Number, required: true},
    image: {type: String},
    creatorId:[{type:mongoose.Types.ObjectId, ref:'User'}],
}, {
    timestamps: true,
});

const Player = mongoose.model('Player', playerSchema);

module.exports = Player;
