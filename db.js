const mongoose = require('mongoose');
require('dotenv').config();
/* const DB_URL = "mongodb://localhost:27017/project-rivas-node"; */
const {DB_URL} = process.env;
/* console.log('db url', process.env.DB_URL); */
const connect = async() => {
    console.log('Testando la base de datos');
    try{
        mongoose.connect(DB_URL, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        });
        console.log('Conectado a la base de datos')
    }catch(error) {
        console.log('Error conectado con la base de datos', error)
    }
}

module.exports = {connect, DB_URL}
