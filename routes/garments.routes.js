const express = require("express");
const garmentsController = require("../controllers/garments.controller");
const Garment = require("../models/Garment");
const router = express.Router();
const {
  upload,
  uploadToCloudinary,
} = require("../middlewares/files.middleware");

router.get("/", [upload.single("store"), uploadToCloudinary], garmentsController.garmentsGet);

module.exports = router;
