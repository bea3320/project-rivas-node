const express = require('express');
const router = express.Router();

router.get('/', (req, res) => {
    return res.render('index', {title: 'Rivas Fútbol Club', user: req.user});
})

module.exports=router;