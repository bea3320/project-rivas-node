const Garment = require("../models/Garment");
/* const Cart = require("../models/Cart"); */

const garmentsGet = async (req, res, next) => {
    try {
      const allGarments = await Garment.find();
      const garments = allGarments.filter((garments ) => garments.clothing !== "Mochila");
      const bag = allGarments.filter((garments ) => garments.clothing === "Mochila");
      return res.render("garments", { garments, bag});
    } catch (error) {
      console.log(error);
      next(new Error(error));
    }
  }

/*   const cartGet = function(req, res){
    const products = new Products({
      clothing: req.body.title,
      description: req.body.description,
      price: req.body.price,
      image: req.body.image
    });
    products.save(function(error, documento){
      if(error){
        res.send('Error al intentar agregar el item.');
      }else{
        res.redirect('/garments');
      }
    });
  }; */

  module.exports = {
    garmentsGet,
    /* cartGet */
  }